// APP
const express = require('express');
const session = require('express-session');
const cookieParser = require('cookie-parser')
const cors = require('cors');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const Uploader = require('express-uploader');
const uuidv4 = require('uuid/v4');
const sha512 = require('js-sha512');

// AUTH
const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy;

// DB
const Db = require('nosql');
const Users = Db.load('db/users.nosql');
const Templates = Db.load('db/templates.nosql');
const Documents = Db.load('db/documents.nosql');



const findInCollection = function(collection, requestFunc) {
  return new Promise((resolve, reject) => {
    collection.find().make(query => {
      requestFunc && requestFunc(query)
      query.callback((err, response) => {
        if(!err) resolve(response)
        else reject(err)
      })
    })
  })
}



console.log("Defining authentication strategy...")
passport.use(new LocalStrategy(
  function(username, password, done) {
    Users.find().make(filter => {
        filter
        .where('username', username)
        .where('password', password)
        .first()
        .callback((err, response) => done(err, response))
    });
  }
));

passport.serializeUser((user, done) => done(null, user.id));

passport.deserializeUser(function(id, done) {
  Users.find().make(filter => {
    filter
    .where('id', id)
    .first()
    .callback((err, response) => done(err, response))
  });
});







console.log("Defining app...")
const sessionOptions = {
  secret: 'haxor password lolz',
  resave: true,
  saveUninitialized : false,
  maxAge: 1000*60*60*24*2
}
const app = express();

app.use(morgan('tiny'));
app.use(cors({origin: 'http://localhost:8080', credentials: true}));
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(session(sessionOptions));
app.use(passport.initialize());
app.use(passport.session());








console.log("Defining routes...");
const ensureLoggedIn = (req, res, next) => {
  if(!req.user) {
    res.sendStatus(403);
    return;
  }
  next();
}

// AUTH
app.post('/login',
  passport.authenticate('local'),
  (req, res) => res.json({message: 'ok'})
)

app.get('/',
  (req, res) => res.json({message: 'Behold The MEVN Stack!'})
)

app.get('/user',
  ensureLoggedIn,
  (req, res) => res.json(req.user && req.user.username)
)

app.get('/logout',
  (req, res) => {req.logout(); res.sendStatus(200)}
)

// TEMPLATES
app.get('/templates',
  ensureLoggedIn,
  async (req, res) => {
    let templates = await findInCollection(Templates, query => query.where("owner", req.user.id));
    res.json(templates);
  }
)

app.post('/templates',
  ensureLoggedIn,
  (req, res) => {
    let name = req.body && req.body.name;
    let newTemplate = {name, id: uuidv4(), owner: req.user.id, fields: []};
    Templates.insert(newTemplate).callback(() => res.json(newTemplate))
  }
)

app.delete('/templates',
  ensureLoggedIn,
  (req, res) => {
    let id = req.body && req.body.id;
    Templates.remove().make(query => {
        query.first()
        query.where('owner', req.user.id);
        query.where('id', id);
        query.callback(() => res.sendStatus(200));
    });
  }
)

app.put('/templates',
  ensureLoggedIn,
  (req, res) => {
    let id = req.body && req.body.id;
    Templates.modify(req.body).make(query => {
      query.where('owner', req.user.id);
      query.where('id', id);
      query.callback(() => res.sendStatus(200));
    })
  }
)


app.get('/documents',
  ensureLoggedIn,
  async (req, res) => {
    let documents = await findInCollection(Documents, query => query.where("owner", req.user.id));
    res.json(documents);
  }
)

// DOCUMENTS
app.post('/documents',
  ensureLoggedIn,
  async (req, res) => {
    let templateId = req.body && req.body.id;
    let template = await findInCollection(Templates, query => {
      query.where('id', templateId);
      query.where('owner', req.user.id);
      query.first();
    })

    let newDocument = {
      id: uuidv4(),
      name: `Document generated from template '${template.name}'`,
      owner: req.user.id,
      fields: template.fields.map(f => ({
        ...f,
        parameters: { ...f.parameters }
      }))
    }
    Documents.insert(newDocument).callback(() => res.json(newDocument))
  }
)

app.delete('/documents',
  ensureLoggedIn,
  (req, res) => {
    let id = req.body && req.body.id;
    Documents.remove().make(query => {
        query.first()
        query.where('owner', req.user.id);
        query.where('id', id);
        query.callback(() => res.sendStatus(200));
    });
  }
)


app.put('/documents',
  ensureLoggedIn,
  (req, res) => {
    let id = req.body && req.body.id;
    console.log(JSON.stringify(req.body));
    Documents.modify(req.body).make(query => {
      query.where('owner', req.user.id);
      query.where('id', id);
      query.callback(() => res.sendStatus(200));
    })
  }
)





const port = process.env.PORT || 4000;

/*
app.all('/upload', function(req, res, next) {
    var uploader = new Uploader({
        debug: true,
        validate: true,
        tmpDir: __dirname + '/tmp',
        publicDir: __dirname + '/public',
        uploadDir: __dirname + '/public/templatefiles',
        uploadUrl: '/templatefiles/'
    });
    uploader.uploadFile(req, function(data) {
        res.send(JSON.stringify(data), {'Content-Type': 'text/plain'}, 200);
    });
});
*/
app.listen(port, () => {
    console.log(`listening on ${port}`);
});
