import Vue from 'vue'
import VueRouter from 'vue-router'

import store from './store/store'

import Auth from './views/Auth'
import Home from './views/Home'
import Templates from './views/Templates'
import Template from './views/Template'
import Documents from './views/Documents'
import Document from './views/Document'

// Workaround for strange vue-router error when navigating with redirection.
const routerPush = VueRouter.prototype.push
VueRouter.prototype.push = function push (location) {
  return routerPush.call(this, location).catch(error => console.log(error))
}

Vue.use(VueRouter)

var router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    { name: 'auth', path: '/auth', component: Auth },
    { name: 'home', path: '/home', component: Home },
    { name: 'templates', path: '/templates', component: Templates },
    { name: 'template', path: '/templates/:id/', component: Template, props: true },
    { name: 'documents', path: '/documents', component: Documents },
    { name: 'document', path: '/documents/:id', component: Document, props: true },
    { path: '*', redirect: '/home' }
  ]
})

router.beforeEach((to, from, next) => {
  console.log(`Trying to navigate to route ${to.name}`)
  if (to.name === 'auth') {
    next()
  } else if (!store.getters['user/isLogged']) {
    console.log('redirect not logged in')
    next('auth')
  } else next()
})

export default router
