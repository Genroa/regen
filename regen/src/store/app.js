export default {
  namespaced: true,
  state: {
    openDrawer: false,
    saving: false
  },

  mutations: {
    setDrawer (state, drawerState) {
      state.openDrawer = drawerState
    }
  }
}
