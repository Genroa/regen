import axios from '@/plugins/axios'

export default {
  namespaced: true,
  state: {
    documents: []
  },

  getters: {

  },

  mutations: {
    updateFieldParameter (state, { doc, fieldIndex, parameterName, newValue }) {
      let document = state.documents.find(d => d === doc);
      document.fields[fieldIndex].parameters[parameterName] = newValue
    }
  },

  actions: {
    async getDocuments ({ state }) {
      let result = await axios.get('/documents')
      state.documents = result.data
    },

    async createDocument ({ state, commit, rootState }, template) {
      rootState.app.saving = true
      let result = await axios.post('/documents', {id: template.id});
      state.documents.push(result.data)
      rootState.app.saving = false
    },

    async deleteDocument ({ state, rootState }, { id }) {
      rootState.app.saving = true
      await axios.delete('/documents', { data: { id: id } })
      state.documents = state.documents.filter(t => t.id !== id)
      rootState.app.saving = false
    },

    async updateDocument ({ state, rootState }, document) {
      rootState.app.saving = true
      await axios.put('/documents', document)
      rootState.app.saving = false
    }
  }
}
