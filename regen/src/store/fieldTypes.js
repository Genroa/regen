export default {
  namespaced: true,
  state: {
    fieldTypes: {},
    categories: []
  },

  mutations: {
    registerFieldType (state, { id, description }) {
      state.fieldTypes[id] = description
    },

    registerCategory (state, category) {
      if (!state.categories.find(c => c === category)) state.categories.push(category)
    }
  },

  actions: {
    registerFieldType ({ state, commit }, { id, description }) {
      commit('registerFieldType', { id, description })
      for (let category of description.categories) {
        commit('registerCategory', category)
      }
    }
  }
}
