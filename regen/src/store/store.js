import Vue from 'vue'
import Vuex from 'vuex'

import fieldTypes from './fieldTypes'
import app from './app'
import user from './user'
import templates from './templates'
import documents from './documents'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: { fieldTypes, app, user, templates, documents }
})
