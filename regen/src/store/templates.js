import axios from '@/plugins/axios'

export default {
  namespaced: true,
  state: {
    templates: [
      { name: 'Test Template', fields: [{ name: 'Field1', type: 'TextField' }] }
    ]
  },

  getters: {

  },

  mutations: {
    updateFieldName (state, { template, fieldIndex, newName }) {
      state.templates.find(t => t === template).fields[fieldIndex].name = newName.split(' ').join('')
    }
  },

  actions: {
    async getTemplates ({ state }) {
      let result = await axios.get('/templates')
      state.templates = result.data
    },

    async createTemplate ({ state, rootState }, { name }) {
      rootState.app.saving = true
      let result = await axios.post('/templates', { name: name })
      state.templates.push(result.data)
      rootState.app.saving = false
    },

    async deleteTemplate ({ state, rootState }, { id }) {
      rootState.app.saving = true
      await axios.delete('/templates', { data: { id: id } })
      state.templates = state.templates.filter(t => t.id !== id)
      rootState.app.saving = false
    },

    async updateTemplate ({ state, rootState }, template) {
      rootState.app.saving = true
      await axios.put('/templates', template)
      rootState.app.saving = false
    }
  }
}
