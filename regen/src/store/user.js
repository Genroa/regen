import axios from '@/plugins/axios'
import router from '@/router'
import sha512 from 'js-sha512'

export default {
  namespaced: true,
  state: {
    loggingIn: false,
    username: null
  },

  getters: {
    isLogged (state) {
      return state.username !== null
    }
  },

  mutations: {

  },

  actions: {
    retrieveAllUserData ({ dispatch }) {
      dispatch('templates/getTemplates', null, { root: true })
      dispatch('documents/getDocuments', null, { root: true })
    },

    async attemptLogin ({ state, commit, dispatch }, { username, password }) {
      var data = {
        username,
        password: sha512(password)
      }
      state.loggingIn = true

      await axios.postFormUrlEncoded('/login', data)
      localStorage.setItem('username', username)
      state.username = username

      dispatch('retrieveAllUserData')

      console.log('Logged in!')
      router.push('/')
    },

    async reconnect ({ state, dispatch }) {
      let username = localStorage.getItem('username')

      if (!username) throw new Error('No user found clientside')
      let result = await axios.get('/user')

      if (!result.data) throw new Error('No user found serverside')
      if (result.data !== username) throw new Error("Clientside user doesn't match serverside session")

      console.log('Re-Logged in successfully!')
      state.username = result.data

      dispatch('retrieveAllUserData')

      router.push('/')
    },

    logout ({ state, rootState }) {
      axios.get('/logout') // ask to close session
      localStorage.removeItem('username') // forget about the session clientside for future runs
      state.username = null // immediately forget the current user for this session
      state.loggingIn = false
      rootState.app.saving = false
      router.push('/auth') // redirect to auth view
    }
  }
}
