
export default {
  props: ['field'],

  created () {
    if (!this.field) return
    for (let paramName of Object.keys(this.field.parameters)) {
      this.$set(this, paramName, this.field.parameters[paramName])
    }
  },

  computed: {
    fieldTypesDescription () {
      return {
        name: '',
        description: '',
        categories: null,
        parameters: {}
      }
    },

    valueToInject () {
      return ''
    }
  }
}
