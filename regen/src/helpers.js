function mapEntries (obj, mappingFunc) {
  return Object.fromEntries(Object.entries(obj).map(mappingFunc))
}

function formatDate (date) {
  return new Intl.DateTimeFormat('fr-FR', {
    year: 'numeric',
    month: 'numeric',
    day: 'numeric',
    hour: 'numeric',
    minute: 'numeric',
    second: 'numeric',
    hour12: false
  }).format(date)
}

export { mapEntries, formatDate }
