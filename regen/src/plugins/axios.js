import axios from 'axios'
import store from '@/store/store'
import querystring from 'query-string'

const instance = axios.create({
  baseURL: 'http://localhost:4000',
  timeout: 1000,
  headers: {},
  withCredentials: true
})

instance.interceptors.response.use(function (response) {
  // Do something with response data
  return response
}, function (error) {
  console.log('Something when wrong, logging out...')
  store.dispatch('user/logout')
  return Promise.reject(error)
})

instance.postFormUrlEncoded = function (url, form) {
  return instance.post(url, querystring.stringify(form), { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } })
}

instance.postJson = function (url, obj) {
  return instance.post(url, JSON.stringify(obj), { headers: { 'Content-Type': 'application/json' } })
}

export default instance
