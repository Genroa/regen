# Regen

Regen is a documents (docx) generator, based on templates.

# How to install

Like any project, clone the repo, then go into the 
project folders with a command line and type `npm install` (for the client, and the server).

Alternatively, you can import it in Vue UI.

# How to run

The server and the client are two different web applications.

- to start the server, run `node index.js` or `npm run serve` in the root folder.
- to start the client, run `vue-cli-service serve` in the regen folder.

(you should really use Vue UI to manage the client project, it'll make your
life easier.)

# Standard Workflow

1. In the app, authenticate (the only account for now is admin/password)
2. Go in the Templates view
3. create a new template
4. Click on the template in the templates list to edit it
5. Add new variables of the type you wish to use. The description explains what variables are provided to use in the template file
6. Create a new document based on the template
7. Go in the Documents view
8. Click on the newly created document
9. Fill the fields and provide a docx template file
10. Click "Generate"

# Template files ?

Template files are regular files with special syntaxes added to inject values and generate parts of document based on their values. 
For more informations, take a look at theses links:
- [Tag types section in Docxtemplater](https://docxtemplater.readthedocs.io/en/latest/tag_types.html)
- [Angular expressions cheatsheet](http://teropa.info/blog/2014/03/23/angularjs-expressions-cheatsheet.html) (angular expressions are supported)